<?php

require_once(__DIR__ . '/vendor/autoload.php');

$container = new DI\Container();

$container->set('root', __DIR__ . '/src');
$container->set('feed-path', __DIR__ . '/feed-exports');
$container->set(
    'Importer\Domain\Video\VideoRepositoryInterface',
    \DI\create('Importer\Infrastructure\Persistence\Video\VideoRepository')
);
$container->set(
    'Importer\Domain\Video\VideoStorageInterface',
    \DI\create('Importer\Infrastructure\Persistence\Video\VideoStorage')
);
$container->set('strategy-namespace', 'Importer\Domain\Video\Service\\');
