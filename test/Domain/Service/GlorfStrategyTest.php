<?php

use PHPUnit\Framework\TestCase;

/**
 * Class GlorfStrategyTest
 */
class GlorfStrategyTest extends TestCase
{
    public function testRead()
    {
        $glorfStrategy =
            $this->getMockBuilder('Importer\Domain\Video\Service\GlorfStrategy')
            ->setMethods(['getRawContent'])
            ->getMock();
        $glorfStrategy->method('getRawContent')->willReturn($this->getJson());

        /** @var \Importer\Domain\Video\VO\FeedContent $feedContent */
        $feedContent = $glorfStrategy->read("");

        $this->assertCount(3, $feedContent->getVideos());
        $this->assertEquals('science experiment goes wrong', $feedContent->getVideos()[0]->getTitle());
        $this->assertEquals('http://glorf.com/videos/3', $feedContent->getVideos()[0]->getUrl());
        $this->assertCount(3, $feedContent->getVideos()[0]->getTags());

    }

    private function getJson()
    {
        return '{
                    "videos": [
                        {
                            "tags": [
                                "microwave",
                                "cats",
                                "peanutbutter"
                            ],
                            "url": "http://glorf.com/videos/3",
                            "title": "science experiment goes wrong"
                        },
                        {
                            "tags": [
                                "dog",
                                "amazing"
                            ],
                            "url": "http://glorf.com/videos/4",
                            "title": "amazing dog can talk"
                        },
                        {
                            "tags": [
                                "dog",
                                "amazing"
                            ],
                            "url": "http://glorf.com/videos/4",
                            "title": "amazing dog can talk"
                        }
                    ]
                }';
    }
}
