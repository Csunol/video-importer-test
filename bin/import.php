<?php

require_once(__DIR__ . '/../config.php');

if (!isset($argv[1])) {
    echo "Error - Import command needs 1 param"; die;
}

$videoImportCommand = $container->get('Importer\Infrastructure\UI\Command\VideoImportCommand');

$videoImportCommand->execute($argv[1], $container->get('feed-path'));
