Install:

`composer install`

Run application:

`php bin/import.php glorf`

Run tests

`php vendor/bin/phpunit test`


I used to do my applications in symfony. I decided to do this test without framework.
If I had more time, I would have controlled more exceptions and maybe put config in a json or yaml file.