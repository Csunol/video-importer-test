<?php

namespace Importer\Application\UseCase;

use Importer\Domain\Video\Entity\Video;
use Importer\Domain\Video\Service\Downloader;
use Importer\Domain\Video\VideoRepositoryInterface;
use Importer\Domain\Video\VideoStorageInterface;

/**
 * Class ImportCommandHandler
 * @package Importer\Application\UseCase
 */
class ImportCommandHandler
{

    /** @var Downloader */
    private $downloader;

    /** @var VideoRepositoryInterface */
    private $videoRepository;

    /** @var VideoStorageInterface */
    private $videoStorage;

    public function __construct(
        Downloader $downloader,
        VideoRepositoryInterface $videoRepository,
        VideoStorageInterface $videoStorage
    ) {
        $this->downloader = $downloader;
        $this->videoRepository = $videoRepository;
        $this->videoStorage = $videoStorage;
    }

    public function handle(ImportCommand $importCommand)
    {
        $feedContent = $importCommand->strategy()->read($importCommand->getFeedPath());

        /** @var Video $video */
        foreach ($feedContent->getVideos() as $video) {
            $this->output($video);
            $this->videoRepository->save($video);
            $video = $this->downloader->download($video);
            $this->videoStorage->store($video);
        }
    }

    private function output(Video $video)
    {
        $tags = (is_null($video->getTags())) ? '' : implode(',', $video->getTags());

        echo "Importing: " . $video->getTitle() . " Url: " . $video->getUrl() . " Tags: " . $tags . "\n";
    }
}
