<?php

namespace Importer\Application\UseCase;

use Importer\Domain\Video\Service\ReaderStrategyInterface;

/**
 * Class VideoImportCommand
 * @package Importer\Application\UseCase
 */
class ImportCommand
{
    /** @var ReaderStrategyInterface */
    private $strategy;

    /** @var string */
    private $feedPath;

    public function __construct(ReaderStrategyInterface $strategy, string $feedPath)
    {
        $this->strategy = $strategy;
        $this->feedPath = $feedPath;
    }

    public function strategy(): ReaderStrategyInterface
    {
        return $this->strategy;
    }

    public function getFeedPath(): string
    {
        return $this->feedPath;
    }
}
