<?php

namespace Importer\Domain\Video\Service;

use Importer\Domain\Video\Entity\Video;
use Importer\Domain\Video\VO\FeedContent;

/**
 * Class GlorfStrategy
 * @package Importer\Domain\Video\Service
 */
class GlorfStrategy implements ReaderStrategyInterface
{
    CONST FILE = '/glorf.json';

    public function read(string $path)
    {
        $content = (array)json_decode($this->getRawContent($path . self::FILE), true);

        $videos = [];
        foreach ($content['videos'] as $video) {
            $videos[] = new Video($video['title'], $video['url'], $video['tags']);
        }

        return new FeedContent($videos);
    }

    public function getRawContent(string $path)
    {
        return file_get_contents($path);
    }
}