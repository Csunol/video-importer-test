<?php

namespace Importer\Domain\Video\Service;

/**
 * Interface ReaderStrategyInterface
 * @package Importer\Domain\Video\Service
 */
interface ReaderStrategyInterface
{
    public function read(string $path);
}