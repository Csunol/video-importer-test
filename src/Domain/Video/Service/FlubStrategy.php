<?php

namespace Importer\Domain\Video\Service;

use Importer\Domain\Video\Entity\Video;
use Importer\Domain\Video\VO\FeedContent;
use Symfony\Component\Yaml\Yaml;

/**
 * Class FlubStrategy
 * @package Importer\Domain\Video\Service
 */
class FlubStrategy implements ReaderStrategyInterface
{
    CONST FILE = '/flub.yaml';

    public function read(string $path)
    {
        $content = Yaml::parse(file_get_contents($path . self::FILE));

        $videos = [];
        foreach ($content as $video) {
            $labels = (isset($video['labels'])) ? explode(',', $video['labels']) : null;
            $videos[] = new Video($video['name'], $video['url'], $labels);
        }

        return new FeedContent($videos);
    }
}
