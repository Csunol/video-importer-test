<?php

namespace Importer\Domain\Video\Service;

use Importer\Domain\Video\Entity\Video;

/**
 * Class Downloader
 * @package Importer\Domain\Video\Service
 */
class Downloader
{
    public function download(Video $video) : Video
    {
        $video->setVideoFile('');

        return $video;
    }
}
