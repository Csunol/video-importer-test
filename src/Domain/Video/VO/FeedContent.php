<?php

namespace Importer\Domain\Video\VO;

use Importer\Domain\Video\Entity\Video;

class FeedContent
{
    /** @var Video[] */
    private $videos;

    /**
     * FeedContent constructor.
     * @param Video[] $videos
     */
    public function __construct(array $videos)
    {
        $this->videos = $videos;
    }

    /**
     * @return Video[]
     */
    public function getVideos(): array
    {
        return $this->videos;
    }
}
