<?php

namespace Importer\Domain\Video;

use Importer\Domain\Video\Entity\Video;

/**
 * Interface VideoStorageInterface
 * @package Importer\Domain\Video
 */
interface VideoStorageInterface
{
    public function store(Video $video);
}