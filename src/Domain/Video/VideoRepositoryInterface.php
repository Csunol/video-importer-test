<?php

namespace Importer\Domain\Video;

use Importer\Domain\Video\Entity\Video;

/**
 * Interface VideoRepositoryInterface
 * @package Importer\Domain\Video
 */
interface VideoRepositoryInterface
{
    public function save(Video $video);
}