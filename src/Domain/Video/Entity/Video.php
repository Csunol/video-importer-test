<?php

namespace Importer\Domain\Video\Entity;

/**
 * Class Video
 * @package Importer\Domain\Video\Entity
 */
class Video
{
    /** @var string */
    private $title;

    /** @var string */
    private $url;

    /** @var array */
    private $tags;

    private $videoFile;

    public function __construct(string $title, string $url, array $tags = null)
    {
        $this->title = $title;
        $this->url = $url;
        $this->tags = $tags;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getTags(): ?array
    {
        return $this->tags;
    }

    public function getVideoFile()
    {
        return $this->videoFile;
    }

    public function setVideoFile($videoFile)
    {
        $this->videoFile = $videoFile;
    }
}
