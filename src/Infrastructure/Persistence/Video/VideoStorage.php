<?php

namespace Importer\Infrastructure\Persistence\Video;

use Importer\Domain\Video\Entity\Video;
use Importer\Domain\Video\VideoStorageInterface;

/**
 * Class VideoStorage
 * @package Importer\Infrastructure\Persistence\Video
 */
class VideoStorage implements VideoStorageInterface
{

    public function store(Video $video)
    {
        return;
    }
}
