<?php

namespace Importer\Infrastructure\UI\Command;

use DI\Container;
use Importer\Application\UseCase\ImportCommand;
use Importer\Application\UseCase\ImportCommandHandler;

/**
 * Class VideoImportCommand
 * @package Importer\Infrastructure\UI\Command
 */

class VideoImportCommand
{
    /** @var ImportCommandHandler */
    private $importCommandHandler;

    /** @var Container */
    private $container;

    public function __construct(ImportCommandHandler $importCommandHandler, Container $container)
    {
        $this->importCommandHandler = $importCommandHandler;
        $this->container = $container;
    }

    public function execute(string $provider, string $feedPath)
    {
        $strategy = $this->setStrategy($provider);

        $this->importCommandHandler->handle(new ImportCommand($strategy, $feedPath));
    }

    private function setStrategy(string $provider)
    {
        try {
            return $this->container->make(
                $this->container->get('strategy-namespace') . ucfirst($provider) . 'Strategy'
            );
        } catch (\Exception $e) {
            echo 'Invalid provider';
        }
    }


}